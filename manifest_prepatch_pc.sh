#!/bin/bash
# -*- coding: utf-8; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-

# autopatch.sh: script to manage patches on top of repo
# Copyright (c) 2018, Intel Corporation.
# Author: sgnanase <sundar.gnanasekaran@intel.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

top_dir=`pwd`
rompath=$(pwd)
vendor_path="android-generic"
utils_dir="$top_dir/vendor/$vendor_path"
#~ patch_dir="$utils_dir/patches/google_diff/pc_vendor_prepatches/stock"

echo "Searching for vendor prepatches..."
echo -e ${CL_CYN}""${CL_RST}
while IFS= read -r agvendor_name; do
if [ -d $rompath/vendor/$agvendor_name/ ]; then
  romname=$agvendor_name
  echo "Found $agvendor_name!!"
  echo -e ${CL_CYN}""${CL_RST}
  echo -e ${CL_CYN}"applying $agvendor_name specific manifest patch..."${CL_RST}
  patch_dir="$utils_dir/patches/google_diff/pc_vendor_patches/$agvendor_name/manifest_patches"
else
  echo ""
fi
done < $rompath/vendor/$vendor_path/pc_roms.lst


private_utils_dir="$top_dir/vendor/$vendor_path/PRIVATE"
private_patch_dir="$private_utils_dir/patches/google_diff/$TARGET_PRODUCT"

current_project=""
previous_project=""
conflict=""
conflict_list=""

apply_patch() {

  pl=$1
  pd=$2

  echo ""
  echo "Applying Vendor Patches"

  for i in $pl
  do
    current_project=`dirname $i`
    if [[ $current_project != $previous_project ]]; then
      echo ""
      echo ""
      echo "Trying project $current_project"
    fi
    previous_project=$current_project
    
    if [ -d $top_dir/$current_project ] 
    then
		cd $top_dir/$current_project
		#~ cd $rompath/.repo/manifests
		
		
		cd $top_dir/$current_project
		#~ cd $rompath/.repo/manifests
		a=`grep "Date: " $pd/$i`
		b=`echo ${a#"Date: "}`
		c=`git log --pretty=format:%aD | grep "$b"`

		if [[ "$c" == "" ]] ; then
		  git am -3 $pd/$i >& /dev/null
		  if [[ $? == 0 ]]; then
			echo "        Applying          $i"
		  else
			echo "        Conflicts          $i"
			git am --abort >& /dev/null
			conflict="y"
			conflict_list="$current_project $conflict_list"
		  fi
		else
		  echo "        Already applied         $i"
		fi
    else
		echo "Project $current_project does not exist"
    fi

  done
}

#Apply common patches
cd $patch_dir
patch_list=`find .repo/manifests/* -iname "*.patch" | sort -u`
apply_patch "$patch_list" "$patch_dir"
#~ apply_patch "$patch_list"

#Apply Embargoed patches if exist
if [[ -d "$private_patch_dir" ]]; then
    echo ""
    echo "Embargoed Patches Found"
    cd $private_patch_dir
    private_patch_list=`find * -iname "*.patch" | sort -u`
    apply_patch "$private_patch_list" "$private_patch_dir"
fi

echo ""
if [[ "$conflict" == "y" ]]; then
  echo "==========================================================================="
  echo "           ALERT : Conflicts Observed while patch application !!           "
  echo "==========================================================================="
  for i in $conflict_list ; do echo $i; done | sort -u
  echo "==========================================================================="
  echo "WARNING: Please resolve Conflict(s). You may need to re-run build..."
  # return 1
else
  echo "==========================================================================="
  echo "           INFO : All patches applied fine !!                              "
  echo "==========================================================================="
fi
